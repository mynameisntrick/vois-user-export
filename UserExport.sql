SELECT  
  VOIS_SYSTEM.p_sys.org_oe.name as verwaltungseinheit,
	VOIS_SYSTEM.p_sys.org_benutzer.username as username,
	VOIS_SYSTEM.p_sys.org_benutzer.username+'#2023' as passwort,
	VOIS_SYSTEM.p_sys.org_benutzer.status as status,
	COALESCE(logon_von.property_value,'') as 'logon_von',
	COALESCE(logon_bis.property_value,'') as 'logon_bis',
	COALESCE(anrede.property_value,'') as 'anrede',
	COALESCE(vorname.property_value,'') as 'vorname',
  COALESCE(nachname.property_value,'') as 'nachname',
  COALESCE(unterschrift.property_value,'') as 'unterschrift',
  COALESCE(initialen.property_value,'') as 'initialen',
  COALESCE(telefon.property_value,'') as 'telefon',
  COALESCE(telefon_2.property_value,'') as 'telefon_2',
  COALESCE(fax.property_value,'') as 'fax',
  COALESCE(email.property_value,'') as 'email',
	COALESCE(VOIS_SYSTEM.p_sys.org_benutzer.bemerkung,'') as 'bemerkung'
FROM 
  VOIS_SYSTEM.p_sys.org_benutzer
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_oe 
ON 
  VOIS_SYSTEM.p_sys.org_benutzer.oe=VOIS_SYSTEM.p_sys.org_oe.om
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as logon_von
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=logon_von.benutzer AND logon_von.property_name='logon_von'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as logon_bis
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=logon_bis.benutzer AND logon_bis.property_name='logon_bis'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as anrede
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=anrede.benutzer AND anrede.property_name='anrede'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as vorname
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=vorname.benutzer AND vorname.property_name='vorname'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as nachname
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=nachname.benutzer AND nachname.property_name='nachname'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as unterschrift
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=unterschrift.benutzer AND unterschrift.property_name='unterschrift'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as initialen
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=initialen.benutzer AND initialen.property_name='initialen'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as telefon
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=telefon.benutzer AND telefon.property_name='telefon'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as telefon_2
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=telefon_2.benutzer AND telefon_2.property_name='telefon_2'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as fax
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=fax.benutzer AND fax.property_name='fax'
LEFT JOIN 
  VOIS_SYSTEM.p_sys.org_benutzer_properties as email
ON
  VOIS_SYSTEM.p_sys.org_benutzer.om=email.benutzer AND email.property_name='email'