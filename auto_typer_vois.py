"""Autotyper for the Creation of VOIS Users from exported CSV dump"""
import csv
import time
from datetime import datetime
from pynput import keyboard
from pyautogui import typewrite


# The Autotyper class is used for automating typing tasks.
class Autotyper:
    """Autotyper Class for the Creation of VOIS Users from exported CSV dump"""

    # This list will be used to store
    # the user data read from the CSV file.
    user = []
    # This variable is used to prevent multiple key presses from being registered at
    # the same time. When a key is pressed, the `lock` variable is set to `True`, and when the key
    # is released, the `lock` variable is set back to `False`. This ensures that only one action
    # is performed for each key press.
    lock = False
    # This variable is used to keep track of the currently selected user in the `user` list. It
    # is used to navigate through the list of users when the left or right arrow keys are pressed.
    cur_user = 0

    def print_selected(self):
        """
        The function "print_selected" prints the currently selected username and the corresponding
        department.
        """
        print("Aktuell ausgewählt:")
        print(f'\tBenutzername:\t\t{self.user[self.cur_user]["username"]}')
        print(
            f'\tVerwaltungseinheit:\t{self.user[self.cur_user]["verwaltungseinheit"]}\n'
        )

    def sub_one(self):
        """subtracts 1 from cur_user"""
        if self.cur_user <= 0:
            return
        self.cur_user -= 1

    def add_one(self):
        """adds 1 to cur_user"""
        if self.cur_user >= (len(self.user) - 1):
            return
        self.cur_user += 1

    def type_at(self):
        """
        The function types the character "@" by simulating the key combination of
        pressing the right Alt key and then tapping the "q" key.
        """
        time.sleep(0.01)
        keyboard.Controller().press(keyboard.Key.alt_gr)  # @
        keyboard.Controller().tap("q")
        keyboard.Controller().release(keyboard.Key.alt_gr)
        time.sleep(0.01)

    def create_user_first_page(self):
        """
        The function `create_user_first_page` is used to automate the process of creating a user's
        first page by typing in the username, password, and then pressing the
        necessary keys to set the user to change their password and create the user.
        """
        # First Page
        typewrite(self.user[self.cur_user]["username"])  # type username
        keyboard.Controller().tap(keyboard.Key.tab)
        keyboard.Controller().tap(keyboard.Key.tab)  # skip ID
        typewrite(self.user[self.cur_user]["passwort"])  # type password
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["passwort"])  # confirm password
        keyboard.Controller().tap(keyboard.Key.tab)
        keyboard.Controller().tap(keyboard.Key.space)  # set user must change password
        keyboard.Controller().tap(keyboard.Key.tab)
        keyboard.Controller().tap(keyboard.Key.enter)  # press create
        time.sleep(0.01)

    def create_user_info_page(self):
        """
        The function `create_user_info_page` types information into various fields
        on a user info page.
        """
        # Info Page
        keyboard.Controller().tap(keyboard.Key.tab)
        keyboard.Controller().tap(keyboard.Key.tab)
        keyboard.Controller().tap(keyboard.Key.tab)  # skip to status
        for _ in range(int(self.user[self.cur_user]["status"])):
            keyboard.Controller().tap(keyboard.Key.down)  # select status
        keyboard.Controller().tap(keyboard.Key.tab)
        logon_von = ""
        if self.user[self.cur_user]["logon_von"] != "":
            dt_obj = datetime.strptime(self.user[self.cur_user]["logon_von"], "%Y%m%d")
            logon_von = dt_obj.strftime("%d.%m.%Y")  # convert date to proper format
        typewrite(logon_von)
        keyboard.Controller().tap(keyboard.Key.tab)
        logon_bis = ""
        if self.user[self.cur_user]["logon_bis"] != "":
            dt_obj = datetime.strptime(self.user[self.cur_user]["logon_bis"], "%Y%m%d")
            logon_bis = dt_obj.strftime("%d.%m.%Y")  # convert date to proper format
        typewrite(logon_bis)
        keyboard.Controller().tap(keyboard.Key.tab)
        match self.user[self.cur_user]["anrede"]:
            case "Herr":
                keyboard.Controller().tap(keyboard.Key.down)
            case "Frau":
                keyboard.Controller().tap(keyboard.Key.down)
                keyboard.Controller().tap(keyboard.Key.down)
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["vorname"])  # type Vorname
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["nachname"])  # type Nachname
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["unterschrift"])  # type unterschrift
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["initialen"])  # type initialen
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["telefon"])  # type Telefon
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["telefon_2"])  # type Telefon 2
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["fax"])  # type fax
        keyboard.Controller().tap(keyboard.Key.tab)
        mail = self.user[self.cur_user]["email"].split("@")
        typewrite(mail[0])  # type email
        self.type_at()
        typewrite(mail[1])  # type email
        keyboard.Controller().tap(keyboard.Key.tab)
        typewrite(self.user[self.cur_user]["bemerkung"])  # type Bemerkung
        keyboard.Controller().tap(keyboard.Key.tab)

    def on_press(self, key):
        """
        The function `on_press` checks for key presses and performs different actions
        based on the key pressed.

        :param key: The `key` parameter in the `on_press` function represents the key
        that was pressed on the keyboard.
        It is used to determine which action to perform based on the key that was pressed
        :return: In the given code, the function `on_press` does not explicitly return any value.
        If the condition `self.lock` is true, the function will return without
        executing any further code.
        If none of the conditions in the if statements are met, the function will also
        return without executing any further code.
        """
        if self.lock:  # check if button was presses before release
            return
        self.lock = True
        if key == keyboard.Key.f2:
            self.create_user_first_page()
        if key == keyboard.Key.f3:
            self.create_user_info_page()
            print("Bitte Eingaben prüfen")
            print("-------------------------------------------------")
            self.add_one()
            self.print_selected()
        if key == keyboard.Key.f4:
            self.create_user_first_page()
            self.create_user_info_page()
            print("Bitte Eingaben prüfen")
            print("-------------------------------------------------")
            self.add_one()
            self.print_selected()
        if key == keyboard.Key.left:
            self.sub_one()
            self.print_selected()
        if key == keyboard.Key.right:
            self.add_one()
            self.print_selected()

    def on_release(self, key):
        """
        The function `on_release` is a callback function that is triggered when a key is released,
        and it checks if the released key is the escape key to stop the listener.

        :param key: The `key` parameter is the key that was released on the keyboard. It is an
        object of the `keyboard.Key` class, which represents a specific key on the keyboard
        :return: a boolean value. If the key pressed is not the escape key, it will return True.
        If the escape key is pressed, it will return False.
        """
        self.lock = False
        if key == keyboard.Key.esc:
            # Stop listener
            return False
        return True

    def __init__(self):
        """
        The above function reads data from a CSV file, stores it in a list, prints selected data,
        and listens for keyboard events.
        """
        with open("dump.csv", encoding="utf-8") as csv_file:
            reader = csv.DictReader(csv_file, delimiter=";")
            for row in reader:
                self.user.append(row)
        self.print_selected()
        # Collect events until released
        with keyboard.Listener(
            on_press=self.on_press, on_release=self.on_release  # type: ignore
        ) as listener:
            listener.join()


x = Autotyper()
